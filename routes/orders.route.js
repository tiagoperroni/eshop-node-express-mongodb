const { Order } = require('../models/order');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const ordertList = await Order.find();

    if(!ordertList[0]){
        res.status(404).json({ message: 'No one product was found!'})
    }
    res.status(200).json(ordertList)
});

module.exports = router;
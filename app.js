const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');

require('dotenv/config');
const api = process.env.API_URL;

//middleware
app.use(express.json());
app.use(morgan('tiny'));

//routers
const categoriesRoutes = require('./routes/categories.route');
const productRoutes = require('./routes/products.route');
const usersRoutes = require('./routes/users.route');
const orderRoutes = require('./routes/orders.route');


app.use(`${api}/categories`, categoriesRoutes);
app.use(`${api}/products`, productRoutes);
app.use(`${api}/users`, usersRoutes);
app.use(`${api}/orders`, orderRoutes);

//Database
mongoose.connect(process.env.CONNECTION_STRING)
.then(() => {
    console.log('Database connection is ready');
})
.catch((err) => {
    console.log(err);
});

const port = 3001;
app.listen(port, () => {   
    console.log(`Server is running on por ${port}`);
});
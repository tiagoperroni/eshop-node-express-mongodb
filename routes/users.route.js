const { User } = require('../models/category');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const userList = await User.find();

    if(!userList[0]){
        res.status(404).json({ message: 'No one product was found!'})
    }
    res.status(200).json(userList)
});

module.exports = router;
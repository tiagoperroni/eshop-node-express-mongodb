const { Category } = require('../models/category');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const categorieList = await Category.find();

    if(!categorieList[0]){
        res.status(404).json({ message: 'No one product was found!'})
    }
    res.status(200).json(categorieList)
});

module.exports = router;
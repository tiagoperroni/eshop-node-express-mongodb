const {Product} = require('../models/product');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const productList = await Product.find();
    if(!productList[0]){
        res.status(404).json({ message: 'No one product was found!'})
    }
    res.status(200).json(productList)
});

router.post('/', (req, res) => {
    const product = new Product({
        name: req.body.name,
        image: req.body.image,
        countInStock: req.body.countInStock
    });

    product.save().then((createdProduct => {
        res.status(201).json(createdProduct)
    })).catch((err) => {
        res.status(500).json({
            error: err.message,
            sucess: false
        });
    });
   
});

module.exports = router;